﻿using Microsoft.EntityFrameworkCore;
using RoomService.Data.Models;

namespace RoomService.Data
{
   public class DataContext : DbContext
   {
      public DbSet<Room> Rooms { get; set; }
      public DbSet<FireEscape> FireEscapes { get; set; }
     
      protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
      {
         optionsBuilder.UseSqlServer(@"Data Source=localhost;Initial Catalog=RoomService;Persist Security Info=True;User ID=sa;Password=nico93;Pooling=False;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=True");
      }

      protected override void OnModelCreating(ModelBuilder modelBuilder)
      {
         modelBuilder.Entity<Room>()
            .HasIndex(room => new { room.RoomNumber, room.FloorNumber})
            .IsUnique(true);

         modelBuilder.Entity<FireEscape>()
            .HasIndex(fre => new { fre.Code })
            .IsUnique(true);
      }
   }
}
