﻿using Microsoft.EntityFrameworkCore;
using RoomService.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoomService.Data.Repositories
{
   public static class FireEscapeRepository
   {
      public static async Task<FireEscape> FindByIdAsync(this DbSet<FireEscape> repository, long id)
      {
         FireEscape fireEscape = await repository
           .Where(fre => fre.Id == id)
           .SingleOrDefaultAsync();

         return (fireEscape);
      }

      public static async Task<FireEscape> FindByCodeAsync(this DbSet<FireEscape> repository, string code)
      {
         FireEscape fireEscape = await repository
           .Where(fre => fre.Code.Trim().ToLower() == code.Trim().ToLower())
           .SingleOrDefaultAsync();

         return (fireEscape);
      }
   }
}
