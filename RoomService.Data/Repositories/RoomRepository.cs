﻿using Microsoft.EntityFrameworkCore;
using RoomService.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RoomService.Data.Repositories
{
   public static class RoomRepository
   {
      public static async Task<Room> FindByIdAsync(this DbSet<Room> repository, long id)
      {
         Room room = await repository
           .Where(rm => rm.Id == id)
           .Include(rm => rm.FireEscape)
           .SingleOrDefaultAsync();

         return (room);
      }

      public static async Task<List<Room>> GetListAsync(this DbSet<Room> repository, int? floorNumber, int? roomNumber)
      {
         IQueryable<Room> query = repository
                    .Include(rm => rm.FireEscape);

         if (floorNumber != null)
         {
            query = query.Where(rm => rm.FloorNumber == floorNumber);
         }

         if (roomNumber != null)
         {
            query = query.Where(rm => rm.RoomNumber == roomNumber);
         }

         List<Room> result = await query
            .ToListAsync();

         return (result);
      }
   }
}
