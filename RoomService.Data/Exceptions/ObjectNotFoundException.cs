﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RoomService.Data.Exceptions
{
   public class ObjectNotFoundException : Exception
   {
      public ObjectNotFoundException(string message) : base(message) { }
   }
}
