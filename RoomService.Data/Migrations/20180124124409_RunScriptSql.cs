﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace RoomService.Data.Migrations
{
    public partial class RunScriptSql : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
         //Insert fire escapes
         migrationBuilder.Sql("INSERT INTO FireEscapes (Code, Location) VALUES ('EXT1','The main entrance')");
         migrationBuilder.Sql("INSERT INTO FireEscapes (Code, Location) VALUES ('EXT2','At the end of the corridor next to the coffee machine.')");
         migrationBuilder.Sql("INSERT INTO FireEscapes (Code, Location) VALUES ('EXT3','At the end of the corridor next to the toilets.')");


         //Insert rooms
         migrationBuilder.Sql("INSERT INTO Rooms (FireEscapeId, FloorNumber, RoomNumber) VALUES (1, 0, 1)");
         migrationBuilder.Sql("INSERT INTO Rooms (FireEscapeId, FloorNumber, RoomNumber) VALUES (1, 0, 2)");

         migrationBuilder.Sql("INSERT INTO Rooms (FireEscapeId, FloorNumber, RoomNumber) VALUES (2, 1, 1)");
         migrationBuilder.Sql("INSERT INTO Rooms (FireEscapeId, FloorNumber, RoomNumber) VALUES (2, 1, 2)");
         migrationBuilder.Sql("INSERT INTO Rooms (FireEscapeId, FloorNumber, RoomNumber) VALUES (2, 1, 3)");

         migrationBuilder.Sql("INSERT INTO Rooms (FireEscapeId, FloorNumber, RoomNumber) VALUES (3, 2, 1)");
         migrationBuilder.Sql("INSERT INTO Rooms (FireEscapeId, FloorNumber, RoomNumber) VALUES (3, 2, 2)");
         migrationBuilder.Sql("INSERT INTO Rooms (FireEscapeId, FloorNumber, RoomNumber) VALUES (3, 2, 3)");
      }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
