﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RoomService.Data.Models
{
   public class FireEscape
   {
      public long Id { get; set; }


      [Required]
      [MinLength(1)]
      [MaxLength(5)]
      public string Code { get; set; }

      [Required]
      [MinLength(5)]
      [MaxLength(200)]
      public string Location { get; set; }



      public List<Room> Rooms { get; set; }
   }
}
