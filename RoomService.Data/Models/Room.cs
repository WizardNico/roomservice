﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RoomService.Data.Models
{
   public class Room
   {
      public long Id { get; set; }


      [Required]
      public int RoomNumber { get; set; }

      [Required]
      public int FloorNumber { get; set; }



      public long FireEscapeId { get; set; }
      public FireEscape FireEscape { get; set; }
   }
}
