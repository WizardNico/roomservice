﻿using AgileMQ.Interfaces;
using RoomService.Directories.PervasiveBuilding.Room;
using System;
using System.Collections.Generic;
using DataModels = RoomService.Data.Models;
using AgileMQ.Containers;
using AgileMQ.DTO;
using System.Threading.Tasks;
using RoomService.Data;
using RoomService.Utilities;
using RoomService.Data.Exceptions;
using RoomService.Data.Repositories;
using AgileMQ.Extensions;

namespace RoomService.Subscribers
{
   public class RoomSubscriber : IAgileSubscriber<Room>
   {
      public IAgileBus Bus { get; set; }

      public async Task<Room> GetAsync(Room model, MessageContainer container)
      {
         DataContext ctx = container.Resolve<DataContext>();

         DataModels.Room room = await ctx.Rooms.FindByIdAsync(model.Id.Value);

         if (room == null)
            throw new ObjectNotFoundException("Room not found, a wrong ID was used.");

         model = Mapper.Map(room);

         return (model);
      }

      public async Task<Room> PostAsync(Room model, MessageContainer container)
      {
         DataContext ctx = container.Resolve<DataContext>();

         DataModels.FireEscape fireEscape = await ctx.FireEscapes.FindByIdAsync(model.FireEscapeId.Value);

         if (fireEscape == null)
            throw new ObjectNotFoundException("Fire escape not found, a wrong ID was used.");

         //Cretion of new room record
         DataModels.Room room = new DataModels.Room()
         {
            FloorNumber = model.FloorNumber.Value,
            RoomNumber = model.RoomNumber.Value,
            FireEscape = fireEscape,
            FireEscapeId = fireEscape.Id
         };

         //Add the new record to DB adn save changes
         await ctx.Rooms.AddAsync(room);
         await ctx.SaveChangesAsync();

         model = Mapper.Map(room);

         return (model);
      }

      public async Task PutAsync(Room model, MessageContainer container)
      {
         DataContext ctx = container.Resolve<DataContext>();

         DataModels.Room room = await ctx.Rooms.FindByIdAsync(model.Id.Value);
         if (room == null)
            throw new ObjectNotFoundException("Room not found, a wrong ID was used.");

         DataModels.FireEscape fireEscape = await ctx.FireEscapes.FindByIdAsync(model.FireEscapeId.Value);
         if (fireEscape == null)
            throw new ObjectNotFoundException("Fire escape not found, a wrong ID was used.");

         room.FloorNumber = model.FloorNumber.Value;
         room.RoomNumber = model.RoomNumber.Value;
         room.FireEscapeId = model.FireEscapeId.Value;
         room.FireEscape = fireEscape;

         await ctx.SaveChangesAsync();
      }

      public async Task DeleteAsync(Room model, MessageContainer container)
      {
         DataContext ctx = container.Resolve<DataContext>();

         DataModels.Room room = await ctx.Rooms.FindByIdAsync(model.Id.Value);

         if (room == null)
            throw new ObjectNotFoundException("Room not found, a wrong ID was used.");

         ctx.Rooms.Remove(room);
         await ctx.SaveChangesAsync();
      }

      public async Task<List<Room>> GetListAsync(Dictionary<string, object> filter, MessageContainer container)
      {
         DataContext ctx = container.Resolve<DataContext>();

         int? floorNumber = filter.Get<int?>("floorNumber");
         int? roomNumber = filter.Get<int?>("roomNumber");

         List<Room> list = new List<Room>();

         List<DataModels.Room> roomList = await ctx.Rooms.GetListAsync(floorNumber, roomNumber);

         foreach (DataModels.Room room in roomList)
         {
            list.Add(Mapper.Map(room));
         }

         return (list);
      }

      public Task<Page<Room>> GetPagedListAsync(Dictionary<string, object> filter, long pageIndex, int pageSize, MessageContainer container)
      {
         throw new NotImplementedException();
      }
   }
}
