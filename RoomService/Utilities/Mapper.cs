﻿using PervasiveRoomModels = RoomService.Directories.PervasiveBuilding.Room;
using DataModels = RoomService.Data.Models;
using System.Collections.Generic;
using RoomService.Directories.PervasiveBuilding.Room.Shared;

namespace RoomService.Utilities
{
   public class Mapper
   {
      public static PervasiveRoomModels.Room Map(DataModels.Room room)
      {
         return new PervasiveRoomModels.Room()
         {
            Id = room.Id,
            FloorNumber = room.FloorNumber,
            RoomNumber = room.RoomNumber,
            FireEscapeId = room.FireEscapeId,
            FireEscape = new FireEscape() { Code = room.FireEscape.Code, Location = room.FireEscape.Location }
         };
      }
   }
}

