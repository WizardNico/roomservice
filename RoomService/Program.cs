﻿using AgileMQ.Bus;
using AgileMQ.Enums;
using AgileMQ.Interfaces;
using RoomService.Data;
using RoomService.Logger;
using RoomService.Subscribers;
using System;

namespace RoomService
{
   public class Program
   {
      static void Main(string[] args)
      {
         using (IAgileBus bus = new RabbitMQBus("HostName=localhost;Port=5672;UserName=guest;Password=guest;Timeout=3;RetryLimit=3;PrefetchCount=50;AppId=RoomService"))
         {
            bus.Logger = new ConsoleLogger();

            bus.Container.Register<DataContext, DataContext>(InjectionScope.Message);

            bus.Suscribe(new RoomSubscriber());

            Console.WriteLine("RoomService Ready!");
            Console.WriteLine();
            Console.ReadLine();
         }
      }
   }
}
