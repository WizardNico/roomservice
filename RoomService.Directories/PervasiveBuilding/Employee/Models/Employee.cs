﻿using AgileMQ.Attributes;
using RoomService.Directories.PervasiveBuilding.Employee.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RoomService.Directories.PervasiveBuilding.Employee.Models
{
   [QueuesConfig(Directory = "PervasiveBuilding", Subdirectory = "Employee", ResponseEnabled = true)]
   public class Employee
   {
      [Required]
      public long? Id { get; set; }



      [Required]
      public EmployeeRole? Role { get; set; }

      [Required]
      public string Name { get; set; }

      [Required]
      public string Surname { get; set; }

      [Required]
      [EmailAddress]
      public string Email { get; set; }

      [Required]
      public double? PreferredTemperature { get; set; }



      [Required]
      public long? RoomId { get; set; }
   }
}
