﻿using AgileMQ.Attributes;
using RoomService.Directories.PervasiveBuilding.Alarm.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RoomService.Directories.PervasiveBuilding.Alarm.Models
{
   [QueuesConfig(Directory = "PervasiveBuilding", Subdirectory = "Alarm", ResponseEnabled = true)]
   public class Alarm
   {
      [Required]
      public long? Id { get; set; }



      [Required]
      public AlarmPriority? Priority { get; set; }

      [Required]
      public AlarmType? Type { get; set; }

      public DateTime? Date { get; set; }



      [Required]
      public long? RoomId { get; set; }
   }
}
