﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RoomService.Directories.PervasiveBuilding.Alarm.Enum
{
   public enum AlarmType
   {
      Fire = 10,
      Intrusion = 20,
   }
}
