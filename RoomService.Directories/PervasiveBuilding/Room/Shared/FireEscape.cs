﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace RoomService.Directories.PervasiveBuilding.Room.Shared
{
   public class FireEscape
   {
      [Required]
      [MinLength(1)]
      [MaxLength(5)]
      public string Code { get; set; }

      [MinLength(5)]
      [MaxLength(200)]
      public string Location { get; set; }
   }
}
